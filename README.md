# xtesting

Project used to build ONAP integration xtesting dockers
This project contains the definition of the integration dockers, their
associated testcases.
Thanks to gitlab, the dockers are built and hosted in the built-in docker
registry.

This project also includes a site.yaml file allowing to create your own gate.
