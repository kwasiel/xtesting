# Smoke use cases

## Goal

Possibility test custom PM file by 5gbulkpm test case:

### Configuration

An example of job test.yaml can be found hereafter:

```
apiVersion: batch/v1
kind: Job
metadata:
    name: integration-onap-5gbulkpm
    namespace: onap
spec:
    template:
        spec:
            containers:
            -   env:
                -   name: BULK_PM_MODE
				    value: custom
                -   name: PM_LOG_LEVEL
                    value: NONE
                -   name: FILE_FORMAT_TYPE
                    value: file_format_type
                -   name: FILE_FORMAT_VERSION
                    value: file_format_version
                -   name: PM_FILE_PATH
                    value: /var/lib/xtesting/results/<pm_file>
                -   name: EXPECTED_PM_STR
                    value: /var/lib/xtesting/results/
                -   name: EXPECTED_EVENT_JSON_PATH
                    value: location_of_JSON
                -   name: TAG
                    value: 5gbulkpm
                image: registry.gitlab.com/orange-opensource/lfn/onap/integration/xtesting/smoke-usecases-robot:latest
                imagePullPolicy: Always
                name: functest-onap
                volumeMounts:
                -   mountPath: /etc/localtime
                    name: localtime
                    readOnly: true
                -   mountPath: /share/config
                    name: robot-eteshare
                -   mountPath: /var/lib/xtesting/results/
                    name: robot-save-results
            restartPolicy: Never
            volumes:
            -   hostPath:
                    path: /etc/localtime
                name: localtime
            -   configMap:
                    defaultMode: 493
                    name: onap-robot-eteshare-configmap
                name: robot-eteshare
            -   hostPath:
                    path: /dockerdata-nfs/onap/integration/smoke-usecases-robot/5gbulkpm
                name: robot-save-results
			
				
## Usage

The robot scripts have been planned to be launched from the cluster.
The easiest way to run the test consists in creating a kubernetes job.
You can run it as a sandalone dockers but the endpoints must be adapted
to be reachable.

In that case it is possible run 5gbulkpm test case in custom mode.
It means that it is possible execute custom PM files using environment variable equal to - BULK_PM_MODE=custom.

- [ ] Environment variables:
    -mandatory:
     - BULK_PM_MODE: set on custom allow to executed custom PM files.
	                 If this value will be empty or different than custom then default PM file (available in robot image) will be tested.  
     - FILE_FORMAT_TYPE: 
     - FILE_FORMAT_VERSION:
	 - PM_FILE_PATH: location where custom PM file is stored.
	 - EXPECTED_PM_STR: expected string which should be visible in received data from MR topic.
	 - EXPECTED_EVENT_JSON_PATH: location where received data from MR topic will be saved as a JSON file.
	 - TAG: executed test case
  - optional:
	 - PM_LOG_LEVEL: by default in custom mode all PM details are not logged to robot log files.
	                 The available levels: TRACE, DEBUG, INFO, WARN, ERROR and NONE (default).
	 
- [ ] Configuration file:
      A hostPath volume mounts a directory from the host node's filesystem into your Pod.
      In that example driectory from /dockerdata-nfs/onap/integration/smoke-usecases-robot/5gbulkpm in the local host will be mount
      into driectory /var/lib/xtesting/results/ on Pod.
      It means that custom <pm_file> is stored in 5gbulkpm directory on local host. Additional JSON file created during execution will be saved in the same directory.

### Command

To run the job, just type:

```
  kubectl apply -f test.yaml
```

### Output

This job shall create an euphemeral pod.
The results will be available in the mounter volume and shall contains 3 files in
this case:

- xtesting.log
- report.html
- log.html
- output.xml
- xunit.xml

Additional when environment variable BULK_PM_MODE=custom this job creates and saves received data from MR topic
as a JSON file. The results will be also available in the mounter volume.



